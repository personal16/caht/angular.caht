
import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 
constructor(   public swUpdate: SwUpdate){

  if(this.swUpdate.isEnabled)
  {
    this.swUpdate.available.subscribe(()=> {
        alert("برنامه با قابلیت های جدید ابدیت شد :  )")
        window.location.reload();
    })
  }

}

}
